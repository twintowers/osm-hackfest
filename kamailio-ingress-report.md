# Kamailio charm modification for ingress support report
 
## Charm code changes for enabling ingress for kamailio service

diff with original repository
```
https://github.com/davigar15/kamailio-operator
```

charm.py
```python
68a69
>
70c71
<             "service-hostname": self.config.get("external-url", self.app.name),
---
>             "service-hostname": self.config.get("external-url", self.app.name + "." + self.config["ingress-controller-domain-name"]),
72c73
<             "service-port": 5060,
---
>             "service-port": self.config["service-port"],
73a75,80
>
>         logger.info('self.app.name: %r', self.app.name)
>         logger.info('service-name: %r', ingress_config["service-name"])
>         logger.info('service-hostname: %r', ingress_config["service-hostname"])
>         logger.info('service-port: %r', ingress_config["service-port"])
>
```
config.yaml
```python
17a18,25
>   ingress-controller-domain-name:
>     default: "172.21.18.107.nip.io"
>     type: string
>     description: "ingress controller external domain name"
>   service-port:
>     default: "65535"
>     type: string
>     description: "The port of the service to create an ingress for."
```

## Steps for creating ingress resource for kamailio service:
 
Checking juju debug-log:
 
```
juju models
juju switch kamailio-kdu-{id}
```
```log
unit-kamailio-0: 07:57:53 INFO unit.kamailio/0.juju-log self.app.name: 'kamailio'
unit-kamailio-0: 07:57:53 INFO unit.kamailio/0.juju-log service-name: 'kamailio'
unit-kamailio-0: 07:57:53 INFO unit.kamailio/0.juju-log service-hostname: 'kamailio.172.21.18.107.nip.io'
unit-kamailio-0: 07:57:53 INFO unit.kamailio/0.juju-log service-port: '65535'
```
 
Since we have a new namespace for each new NS instance, we need to deploy nginx-ingress-integrator each time after NS instantiation.
```
juju deploy nginx-ingress-integrator ingress
```
New ingress service has been created at this step:
```
kubectl get service ingress -o wide
ingress   ClusterIP   10.152.183.51   <none>        65535/TCP   85m   app.kubernetes.io/name=ingress
```
but no ingress resource at this point has been configured:
```
kubectl get ingresses
No resources found in kamailio-kdu-9bc54eae-ebbc-4ba0-8767-cf8648adde69 namespace.
```
We need to make relation between ingress service and kamailio service manually in order to create ingress resource for kamailio k8s service
```
juju relate ingress kamailio
```
 
juju debug-log:
```log
unit-ingress-0: 16:50:29 INFO juju.worker.uniter.operation ran "ingress-relation-created" hook (via hook dispatching script: dispatch)
unit-kamailio-0: 16:50:29 INFO juju.worker.uniter.operation ran "ingress-relation-joined" hook (via hook dispatching script: dispatch)
unit-kamailio-0: 16:50:29 INFO juju.worker.uniter.operation ran "ingress-relation-changed" hook (via hook dispatching script: dispatch)
unit-ingress-0: 16:50:29 INFO juju.worker.uniter.operation ran "ingress-relation-joined" hook (via hook dispatching script: dispatch)
unit-ingress-0: 16:50:30 INFO unit.ingress/0.juju-log ingress:0: Service created in namespace kamailio-kdu-9bc54eae-ebbc-4ba0-8767-cf8648adde69 with name kamailio
unit-ingress-0: 16:50:30 INFO unit.ingress/0.juju-log ingress:0: Using ingress class public as it is the cluster's default
unit-ingress-0: 16:50:30 INFO unit.ingress/0.juju-log ingress:0: Ingress created in namespace kamailio-kdu-9bc54eae-ebbc-4ba0-8767-cf8648adde69 with name kamailio-ingress
unit-ingress-0: 16:50:30 INFO juju.worker.uniter.operation ran "ingress-relation-changed" hook (via hook dispatching script: dispatch)
unit-ingress-0: 16:50:31 INFO unit.ingress/0.juju-log ingress:0: Service updated in namespace kamailio-kdu-9bc54eae-ebbc-4ba0-8767-cf8648adde69 with name kamailio
unit-ingress-0: 16:50:31 INFO unit.ingress/0.juju-log ingress:0: Using ingress class public as it is the cluster's default
unit-ingress-0: 16:50:31 INFO unit.ingress/0.juju-log ingress:0: Ingress updated in namespace kamailio-kdu-9bc54eae-ebbc-4ba0-8767-cf8648adde69 with name kamailio-ingress
```
 
This way we got new ingress resource created for kamailio service:
```
kubectl get ingresses
kamailio-ingress   public   kamailio.172.21.18.107.nip.io   127.0.0.1   80
```
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/proxy-body-size: 20m
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
  creationTimestamp: "2022-01-29T16:50:30Z"
  generation: 1
  labels:
    app.juju.is/created-by: ingress
  name: kamailio-ingress
  namespace: kamailio-kdu-9bc54eae-ebbc-4ba0-8767-cf8648adde69
  resourceVersion: "913108"
  selfLink: /apis/networking.k8s.io/v1/namespaces/kamailio-kdu-9bc54eae-ebbc-4ba0-8767-cf8648adde69/ingresses/kamailio-ingress
  uid: de451cab-9bf7-4822-b572-ba61488babb2
spec:
  ingressClassName: public
  rules:
  - host: kamailio.172.21.18.107.nip.io
    http:
      paths:
      - backend:
          service:
            name: kamailio-service
            port:
              number: 65535
        path: /
        pathType: Prefix
status:
  loadBalancer:
    ingress:
    - ip: 127.0.0.1
```

This ingress resource executes on a pre-created ingress controller (nginx) located in ingress namespace - which also serves osm services ingresses.

Part of nginx.conf from ingress controller related to ingress resource for kamailio service:
```nginx
location / {
 
        set $namespace      "kamailio-kdu-f33d6e7e-4e14-4219-ad47-a30f0c38d9dc";
        set $ingress_name   "kamailio-ingress";
        set $service_name   "kamailio-service";
        set $service_port   "65535";
        set $location_path  "/";
```
 
## Testing of external HTTP access to kamailio service through ingress
Response from nginx ingress:
 
```  
curl -k kamailio.172.21.18.107.nip.io
```
 ```html
<h1>502 Bad Gateway</h1>
<hr><center>nginx</center>
```
 
We have got 502 because kamailio doesn't respond on the port as it is supposed to.
 
nginx ingress controller log:
```
kubectl logs nginx-ingress-microk8s-controller-lvvbf -n ingress
```
```log
  2022/01/29 17:13:10 [error] 6712#6712: *21872674 connect() failed (111: Connection refused) while connecting to upstream, client: 172.21.18.1, server: kamailio.172.21.18.107.nip.io, request: "GET /favicon.ico HTTP/2.0",
  upstream: "http://10.1.149.34:65535/favicon.ico", host: "kamailio.172.21.18.107.nip.io", referrer: "https://kamailio.172.21.18.107.nip.io/"
```
 
Local test:
```
ubuntu@hackfest-osm-7:~$ curl -L -k http://10.1.149.59:65535
curl: (7) Failed to connect to 10.1.149.34 port 65535: Connection refused
```
Apparently ingress resource has been applied correctly to ingress controller and root cause is kamailio service's inability to properly respond to HTTP request.
In order to rest this hypothesis lets change ingress resource target service to one of services within the same namespace which does respond to HTTP requests.
This service is modeloperator on port 17017
```
kubectl get service modeloperator -o wide
NAME            TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)     AGE   SELECTOR
modeloperator   ClusterIP   10.152.183.193   <none>        17071/TCP   70m   operator.juju.is/name=modeloperator,operator.juju.is/target=model
```
Local testing (we got this response because nginx terminates TLS sessions by default, but this is good enough for this test case):
 
```
curl 10.152.183.193:17071
Client sent an HTTP request to an HTTPS server.
```
 
Modified ingress resource yaml (kamailio-service -> modeloperator, 65535 -> 17071):
```yaml
spec:
  ingressClassName: public
  rules:
  - host: kamailio.172.21.18.107.nip.io
    http:
      paths:
      - backend:
          service:
            name: modeloperator
            port:
              number: 17071
        path: /
        pathType: Prefix
```
  After applying above configuration let's perform end to end test (from neighbor VM: hackfest-osm-42):
 
```
curl -k https://kamailio.172.21.18.107.nip.io/
Client sent an HTTP request to an HTTPS server.
```
 
So we got the same response as the local response.
If we increase replicas parameter value in modeloperator service config from 1 to 2 then external requests will be served on different pods (10.1.149.60:17071 and 10.1.149.61:17071):
 
```log
172.21.18.107 - - [29/Jan/2022:21:30:41 +0000] "GET / HTTP/2.0" 400 48 "-" "curl/7.68.0" 40 0.000 [kamailio-kdu-f33d6e7e-4e14-4219-ad47-a30f0c38d9dc-modeloperator-17071] []
10.1.149.60:17071 48 0.000 400 ed3a6ede721c46590179453587527b2a
 
172.21.18.107 - - [29/Jan/2022:21:30:42 +0000] "GET / HTTP/2.0" 400 48 "-" "curl/7.68.0" 40 0.000 [kamailio-kdu-f33d6e7e-4e14-4219-ad47-a30f0c38d9dc-modeloperator-17071] []
10.1.149.61:17071 48 0.000 400 28bb16a8914824bfa554b1a258d1ad02
```
 
 
## Additional Information
 
Original kamailio service before ingress relation
```
kubectl get service/kamailio --output yaml
```
```yaml
apiVersion: v1
kind: Service
metadata:
  annotations:
    controller.juju.is/id: 6fbd4acf-2214-4a0a-8fe7-95ac93f4c71e
    juju.is/version: 2.9.17
    model.juju.is/id: cfb6d572-7ed6-47ab-84c3-fdfe417575cb
  creationTimestamp: "2022-01-29T19:54:09Z"
  labels:
    app.kubernetes.io/managed-by: juju
    app.kubernetes.io/name: kamailio
  name: kamailio
  namespace: kamailio-kdu-f33d6e7e-4e14-4219-ad47-a30f0c38d9dc
  resourceVersion: "930394"
  selfLink: /api/v1/namespaces/kamailio-kdu-f33d6e7e-4e14-4219-ad47-a30f0c38d9dc/services/kamailio
  uid: 08308433-9249-49b8-8910-0ce215f5ce3c
spec:
  clusterIP: 10.152.183.5
  clusterIPs:
  - 10.152.183.5
  ports:
  - name: placeholder
    port: 65535
    protocol: TCP
    targetPort: 65535
  selector:
    app.kubernetes.io/name: kamailio
  sessionAffinity: None
  type: ClusterIP
status:
  loadBalancer: {}
```
 
Additional new kamailio-service service after ingress relation
  kubectl get service/kamailio-service --output yaml
```yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: "2022-01-29T20:45:11Z"
  labels:
    app.juju.is/created-by: ingress
  name: kamailio-service
  namespace: kamailio-kdu-f33d6e7e-4e14-4219-ad47-a30f0c38d9dc
  resourceVersion: "934639"
  selfLink: /api/v1/namespaces/kamailio-kdu-f33d6e7e-4e14-4219-ad47-a30f0c38d9dc/services/kamailio-service
  uid: 9100a263-9dca-4e43-a3bf-72e4df22526a
spec:
  clusterIP: 10.152.183.183
  clusterIPs:
  - 10.152.183.183
  ports:
  - name: tcp-65535
    port: 65535
    protocol: TCP
    targetPort: 65535
  selector:
    app.kubernetes.io/name: kamailio
  sessionAffinity: None
  type: ClusterIP
status:
  loadBalancer: {}
```
